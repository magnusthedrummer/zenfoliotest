# Zenfolio Code Test
This repository contains sources for the `zenfoliotest` command line app, developed to meet the requirements for the code test described here: 
https://docs.google.com/document/d/1Rd78ZQx6N-B9o9v7KxjrbfroIT0HlmOEbmtB7VGaxuA/edit#

**Author:** Magnus Martikainen

## Setup
Requires .NET Core 2.2, see: https://dotnet.microsoft.com/download/dotnet-core/2.2

## Running the App
To run the app, in the root folder:
```sh
dotnet run
```

To exit the app, type `quit` at the input prompt (or press Ctrl-C).

## Features
Apart from the default features listed in the code test this app supports:

* Numbers with decimal point
* Numbers with thousand separator
* Negative numbers

## Assumptions
Here are the main assumptions made when developing this app.

**Handling of String input**

* Special Characters are assumed to mean any character except letters a-z, A-Z. This is something I'd have confirmed, and I've encapsulated the behavior into a single function `TextProcessor.isSpecialCharacter()` which can easily be updated to support different requirements.
* Letters are considered cases insensitive, i.e. letter 'A' is counted together with letter 'a'.
* Strings with linebreaks are not supported.


**Handling of Numbers input**

* Entered numbers, output values and and intermediate calculation results are limited to the precision, max and min values supported by the .NET `decimal` type. See: https://docs.microsoft.com/en-us/dotnet/api/system.decimal?view=netframework-4.7.2
* Numbers may use comma as a thousand separator.
* Exponatial notation (e.g. 10e5) is not supported.
* When determining the "mode", if there are multiple different values that occur the max number of times, the output is shown as "none", even though technically all of those values should be listed. This decision was made based on the example output shown in the description. I would explicitly ask to have this clarified as the expected behavior.
* When determining the range, if there is only a single number, the range is shown as "none".
* Given that this is an interactive command line app, high throughput and performance has not been given a lot of attention. In particular the number processing could be optimized further if that should be deemed important for another type of application. Here I instead opted for clean separation of the different computations to make the code easy to read and troubleshoot.


## Other Remarks
I decided to dust off my 8+ years old C# skills for this and try out .NET Core, and I was pleasantly surprised at how fun it was. But given that I've not used C# for a while and am not familiar with recent developments, I'm sure there are some areas that could be coded more elegantly with more elaborate use of language features.

I've deliberately used a lot of functions in the code to demonstrate what I consider good practice and appropriate naming to make the code easy to understand and maintain. Obviously some of this might be considered a bit overkill for a small once-off app like this. But I like to keep it neat.


#### Opinions
Regarding the app requested for the test, I would advice against this type of app altogether. Here is my reasoning:
* A command line application is better suited for batch scripting than for user interaction. 
* Scripting this app is nearly impossible, as it prompts for interactive user input. To work well for scripting all input should be passed as parameters to the app and the app should exit after processing. 
* A better user experience could be achieved with a GUI, e.g. via a browser or a desktop app.

I would also advice against the use of 'quit' as input to abort processing, since this mixes commands and data in the same input. Ending with a simple Ctrl-C is better, it will do the job nicely and will keep the input as plain data. (What if someone wants the character counts for 'quit'?)
