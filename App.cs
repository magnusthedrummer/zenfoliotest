using System;
using System.Collections.Generic;
using System.Linq;

namespace zenfoliotest
{
    public class App
    {
        private TextProcessor textProcessor = new TextProcessor();
        private NumbersProcessor numbersProcessor = new NumbersProcessor();


        public void run() {
            var input = promptForInput();
            while (input != "quit") {
                var words = textProcessor.parseWords(input);
                var numbers = numbersProcessor.parseNumbers(words);
                if (words.Length == numbers.Length) {
                    // numbers only
                    output(numbersProcessor.process(numbers));
                }
                else {
                    output(textProcessor.process(words));
                }
                input = promptForInput();
            }
        }

       
        string promptForInput() {
            Console.Write("Input: ");
            return Console.ReadLine();
        }

       
        void output(string text) {
            Console.WriteLine(text);
        }
    }
}