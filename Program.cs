﻿using System;

namespace zenfoliotest
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new App();
            app.run();
        }
    }
}
