using System;
using System.Collections.Generic;
using System.Linq;

namespace zenfoliotest
{
    public class NumbersProcessor
    {
        public decimal[] parseNumbers(string[] words) {
            try {
                return words.Select(w => decimal.Parse(w)).ToArray();
            }
            catch {
                // something is not a number
                return Array.Empty<decimal>();
            }
        }


        public string process(decimal[] numbers)  {
            try {
                var mean = numbers.Average();
                var median = computeMedian(numbers);
                var mode = stringValueOrNone(computeMode(numbers));
                var range = stringValueOrNone(computeRange(numbers));
                return $"mean = {mean}\nmedian = {median}\nmode = {mode}\nrange = {range}";
            } catch (ArithmeticException x) {
                return $"Numeric computation failed. {x.Message}";
            }
        }


        string stringValueOrNone(decimal? number) {
            if (number is decimal value) {
                return value.ToString();
            }
            return "none";
        }
       


        decimal computeMedian(decimal[] numbers) {
            if (numbers.Length == 0) {
                return 0;
            }
            int count = numbers.Length; 
            var sortedNumbers = numbers.OrderBy(n => n); 
            decimal median; 
            if ((count % 2) == 0) {
                // even count
                var halfIndex = count / 2; 
                median = ((sortedNumbers.ElementAt(halfIndex) + 
                    sortedNumbers.ElementAt(halfIndex - 1)) / 2m); 
            } else {
                // odd count
                var middleIndex = (count - 1) / 2;
                median = sortedNumbers.ElementAt(middleIndex); 
            }
            return median;
        }

       
        decimal? computeMode(decimal[] numbers) {
            if (numbers.Length == 0) {
                return null;
            }
           var sortedGroups = numbers.GroupBy(n => n).OrderByDescending(g => g.Count());
           var it = sortedGroups.GetEnumerator();
           it.MoveNext();
           var firstGroup = it.Current;
           if (it.MoveNext()) {
               var secondGroup = it.Current;
               if (secondGroup.Count() == firstGroup.Count()) {
                   // more than one group have the max count
                   return null;
               }
           }
           return firstGroup.Key;
        }

       
        decimal? computeRange(decimal[] numbers) {
            if (numbers.Length < 2) {
                return null;
            }
            var sortedNumbers = numbers.OrderBy(n => n);
            return sortedNumbers.Last() - sortedNumbers.First();
        }
    }
}