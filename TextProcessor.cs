using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;


namespace zenfoliotest
{
    public class TextProcessor
    {
        public string[] parseWords(string input) {
            return input.Split(' ');
        }


        public string process(string[] words) {
            var charCounts = new Dictionary<char, int>();
            foreach (string word in words) {
                appendCharacterOccurrences(charCounts, word);
            }

            var output = new StringBuilder();

            var chars = charCounts.Keys.OrderBy(c => c);
            foreach (char c in chars) {
                output.Append($"{c}: {charCounts[c]}\n");
            }
            return output.ToString();
        }

       
       void appendCharacterOccurrences(Dictionary<char, int> result, string word) {
            foreach (char c in word.ToLower()) {
                if (isSpecialCharacter(c)) {
                    continue;
                }
                int count = 0;
                result.TryGetValue(c, out count);
                result[c] = count + 1;
            }
        }

       
        bool isSpecialCharacter(char c) {
            return c < 'a' || c > 'z';
        }

    }
}